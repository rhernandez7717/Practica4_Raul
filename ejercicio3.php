<?php
	$un_bool = TRUE;			//valor booleano
	$un_str = "Programación";  //valor cadena
	$u_str2 = 'Programación';  //una cadena
	$un_int = 12;				//un entero
	
	echo gettype($un_bool);  //imprime:boleano
	echo gettype($un_str);	//imprime:string
	
	//si este valor es un entero, incrementarlo en cuatro
	if(is_int($un_int)){
		$un_int +=4;
	}
	//si $bool es una cadena, imprimirla
	// (no imprime nada)
	if (is_string($un_bool)){
		echo "cadena": $un_bool";
	}

?>